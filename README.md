# CI/CD: Advanced workshop

Workshop given at GitLab Contribute 2019: https://gitlab.com/gitlabcontribute/new-orleans/issues/52

## Development

Have [docker installed](https://www.docker.com/products/docker-desktop) on your computer. Inside the root of this project run `docker-compose up`

